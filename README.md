# Four-in-a-row Game
A two-player connection board game written using Vite + React.

## How to run
### Locally
1. Clone this project.
2. `cd` into the project directory.
3. Run `yarn`.
4. Run `yarn dev`.
5. Navigate to `http://127.0.0.1:5173/four-in-a-row-game/` in your web browser.

### Demo
A demo of this project is available [here](https://iankorpi.gitlab.io/four-in-a-row-game/).

## Dependencies
* [Yarn](https://yarnpkg.com/)
* [React](https://reactjs.org/)
* [Immer](https://github.com/immerjs/immer)
* [Vite](https://vitejs.dev/)