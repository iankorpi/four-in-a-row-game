import React, { useState } from 'react'
import styles from '../styles/ComponentStyles.module.css'
import Piece from './Piece.jsx'
import { generateBoardArray } from '../GameLogic.js'

const GameBoard = () => {
    const [boardState, setBoardState] = useState(() => generateBoardArray());

    const [playerState, setPlayerState] = useState(1);
    const [winState, setWinState] = useState(false);
    const [tiedState, setTiedState] = useState(false);

    return (
        <div>
            <div>
                <div className={styles.gameInfo}>
                <p>{`Player ${playerState}'s turn!`}</p>
                                {winState && <p>{`Player ${playerState} wins!`}</p>}
                    {tiedState && <p>The game is tied.</p>}
                </div>
                <div className={styles.board}>
                    {boardState.map((rows, i) =>
                        rows.map((col, j) => <Piece key={`${j}-${i}`} x={j} y={i} boardState={boardState} setBoardState={setBoardState} winState={winState} setWinState={setWinState} playerState={playerState} setPlayerState={setPlayerState} setTiedState={setTiedState}/>))}
                </div>
            </div>
            <div className={styles.reset}>
                    <button className={styles.reset}
                    onClick={function(){
                    setBoardState(generateBoardArray());
                    setPlayerState(1);
                    setWinState(false);
                    setTiedState(false);
                }}>Reset Game</button>
            </div>
        </div>
    );
}
            

export default GameBoard;