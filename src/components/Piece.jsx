import React from 'react'
import { produce } from 'immer';
import styles from '../styles/ComponentStyles.module.css'
import { evaluateWins, evaluateTied, findLowestSpace } from '../GameLogic.js'

const PLAYER_COLORS = [undefined, '#D41159', '#1A85FF']; // color to style the player discs with.
const PLAYER_COLORS_BORDER = ['none', '0.5em double #990038', '0.5em double #0038CC']; // border color

class Piece extends React.Component {
    place() {
        /* Place a piece. */
        let x = this.props.x;
        let y = findLowestSpace(this.props.boardState, x);

        if (this.getPlayerForPiece() != 0 || this.props.winState == true) /* don't place the piece if the space is already occupied or game is won. */
            return;

        const newState = produce(this.props.boardState, newState => {
            newState[y][x] = this.props.playerState;
        });

        this.props.setBoardState(newState);
        if ( evaluateWins(newState, x, y) ) {
            this.props.setWinState(true);
            return;
        }
        if ( evaluateTied(newState) ) {
            this.props.setTiedState(true);
            return;
        }
        this.props.setPlayerState(this.props.playerState ^ 3); // toggle between player = 1 and player = 2;
    }
    
    getPlayerForPiece() {
        /* Get the player the piece belongs to. */

        let y = this.props.y;
        let x = this.props.x;

        if (y >= this.props.boardState.length) {
            return null;
        } else if (x >= this.props.boardState[y].length) {
            return null;
        }
        return this.props.boardState[y][x];
    }

    render() {
        return (
            <button
            className={styles.piece}
            onClick={() => this.place()}
            style={
                {backgroundColor: PLAYER_COLORS[this.getPlayerForPiece()],
                border: PLAYER_COLORS_BORDER[this.getPlayerForPiece()],
            }}>
                {`${this.props.x}, ${this.props.y}`}
            </button>
        )
    }
};

export default Piece;