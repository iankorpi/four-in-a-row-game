import GameBoard from './components/Board.jsx'
import './App.css'

function App() {
  return (
    <div className="App">
      <GameBoard/>
    </div>
  )
}

export default App
