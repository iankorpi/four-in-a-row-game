/* Game logic for four in a row game. */

const nCols = 6;
const nRows = 7;

function generateBoardArray() { /* generate a board array based on the dimensions specified in the above constants */
    const rows = [];
    for (let i = 0; i < nRows; i++) {
        rows.push(Array.from(Array(nCols), () => 0));
    }

    return rows;
}

function findLowestSpace(board, x) { /* find lowest free space in a column */
    for (let i = nCols; i >= 0; i--) {
        if (board[i][x] == 0) {
            return i;
        }
    }
    
    return 0; /* if no other match found... */
}

function checkDiagonals(board, x, y) { /* checks diagonals for win condition */

    const leftBound = Math.max(x-3, 0);
    const rightBound = Math.min(x+3, nCols-1);
    const topBound = Math.max(y-3, 0);
    const bottomBound = Math.min(y+3, nRows-1);


    const value = board[y][x]; // player we are checking for.
    
    // descending diagonals
    let consecutives = 0;
  
    for (let i = 3; i >= -3; i--) {
        if (x+i < leftBound || x+i > rightBound || y+i < topBound || y+i > bottomBound) { // skip spaces that don't exist within our bounds
            continue;
        } else if (board[y+i][x+i] == value) {
            consecutives++;
            console.log(`${i}${i}`);

            if (consecutives == 4) {
                return true;
            }
        } else {
            consecutives = 0;
        }
    }

    // ascending diagonals
    consecutives = 0;
    
    for (let i = 3; i >= -3; i--) {
        if (x+i < leftBound || x+i > rightBound || y-i < topBound || y-i > bottomBound) { 
            continue;
        } else if (board[y-i][x+i] == value) {
            consecutives++;
            if (consecutives == 4) {
                return true;
            }
        } else {
            consecutives = 0;
        }
    }

    return false;
}

function checkRow(board, x, y) { // check for across wins
    const value = board[y][x];

    if ( 
    ( board[y][2] == value && board[y][3] == value) 
    && ( 
        ( board [y][0] == value && board[y][1] == value)
        || (board[y][1] == value && board[y][4] == value) 
        || (board[y][4] == value && board[y][5] == value))
    ) {
        return true;
    }
    return false; 
    }

function checkColumn(board, x, y) { /* check column for win */
    const value = board[y][x];
    let consecutive = 0;
   
    checkloop: for (let i = 0; i < nRows; i++) {
        if (board[i][x] != value) {
           consecutive = 0;
        } else { consecutive++; };

        if (consecutive == 4) {
            return true;
        }
        
    }

    return false;
}

function evaluateWins(board, x, y) { /* Evaluate an array for wins at a given position */
    return checkDiagonals(board, x, y) + checkColumn(board, x, y) + checkRow(board, x, y);
}

function evaluateTied(board) { /* if board is full, returns true, else false.*/
   return Boolean(board.map(row => row.includes(0)).reduce((acc, val) => acc+ val, 0)) ^ true;
}

export { evaluateWins, evaluateTied, generateBoardArray, findLowestSpace };